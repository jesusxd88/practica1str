package body Visualizer is

   procedure VisualizarSalida (outDB : DataBase) is
   
   begin
      
      Put_Line("k      ST1(k)      ST2(k)     ST3(k)     ST4(k)       SC1(k)     SC2(k)     SR1(k)     SD1(k)");  
      Put_Line("");
      
      --Iteramos sobre la base de datos y la mostramos por consola
      for i in 1..15 loop
         for j in 1..9 loop
            Put(Fixed(outDB(i, j))'Image & "   ");
         end loop;
         Put_Line("");
      end loop;
      
   end VisualizarSalida;

end Visualizer;
