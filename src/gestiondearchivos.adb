package body GestionDeArchivos is

   --Iteramos sobre el archivo indicado y lo guardamos en una matriz de entrada de datos
   function FileParser (FileName : String) return InputDB is

      InputFile : File_Type;
      DB        : InputDB;

   begin

      Open (InputFile, In_File, FileName);
      
      for i in 1 .. 15 loop
         for j in 1 .. 4 loop
            Get (InputFile, DB (i, j));
            if (j = 4) then
               Skip_Line (InputFile);
            end if;
         end loop;
      end loop;

      Close (InputFile);
      return DB;

   end FileParser;

   
   --Iteramos sobre la base de datos y la guardamos en el archivo indicado
   procedure FileWriter (outDB : DataBase; FileName : String) is
    
      OutputFile : File_Type;
   
   begin
   
      Create (OutputFile, Out_File, FileName);
      Close(OutputFile);
      Open (OutputFile, Out_File, FileName);
      

      Put_Line(OutputFile, "k        ST1(k)        ST2(k)        ST3(k)        ST4(k)          SC1(k)        SC2(k)        SR1(k)        SD1(k)");  
      Put_Line(OutputFile, "");
      
      for i in 1..15 loop
         for j in 1..9 loop
            Put(OutputFile, Fixed(outDB(i, j))'Image & "   ");
         end loop;
         Put_Line(OutputFile, "");
      end loop;

      Close (OutputFile);
      
   end FileWriter;

end GestionDeArchivos;
