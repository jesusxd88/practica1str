package body SistemaDeSeguridad is

   --Funcion que nos imprime por consola la alerta
   procedure Alarma (tipo : TipoAlarma; valor: Float) is
       
   begin
      
      Put_Line("");
      Put_Line("���ALERTA!!!"); 

      if tipo = ST2t then
         
         Put_Line("La temperatura de la salida del campo (ST2) es superior a 95�C: " & Fixed(valor)'Image & "�C");

      elsif tipo = ST3t then
         
         Put_Line("La temperatura de la entrada del m�dulo de destilaci�n (ST3) es superior a 95�C: " & Fixed(valor)'Image & "�C");

      end if;

      Put_Line("");
      
   end Alarma;

end SistemaDeSeguridad;
