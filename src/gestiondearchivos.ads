with Ada.Text_IO;       use Ada.Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;

package GestionDeArchivos is

   type InputDB is array(1..15, 1..4) of Float;
   type DataBase is array(0..15,1..9) of Float;
   type Fixed is delta 0.001 range -1.06e10 .. 1.06e10;

	function FileParser(FileName : String) return InputDB;
   procedure FileWriter(outDB : DataBase; FileName : String);

end GestionDeArchivos;
