with Ada.Text_IO, Simulador, Ada.Float_Text_IO, GestionDeArchivos, Visualizer, SistemaDeSeguridad;
use Ada.Text_IO, Simulador, Ada.Float_Text_IO, GestionDeArchivos, Visualizer, SistemaDeSeguridad;

procedure Main is

   db : DataBase; --Base de datos principal
   entrada : InputDB; --Datos que leemos del archivo input.txt

   st1, st2, sd1 : Float; --Variables que usaremos

begin


   --Inicializamos la base de datos
   db := (0 => (1 => 0.0,
                2 => 50.0,       -- ST1(0) = 50
                3 => 60.0,       -- ST2(0) = 60
                others => 0.0),
          others => (1 => 0.0,   -- k
                     2 => 0.0,   -- ST1(k)
                     3 => 0.0,   -- ST2(k)
                     4 => 0.0,   -- ST3(k)
                     5 => 0.0,   -- ST4(k)
                     6 => 20.0,  -- SC1(k)
                     7 => 400.0, -- SC2(k)
                     8 => 0.0,   -- SR1(k)
                     9 => 0.0)); -- SD1(k)

   --Guardamos los datos de entrada en un buffer
   entrada := FileParser ("input.txt");


   --Bucle principal. Aqui leemos la entrada en la DB principal y ejecutamos el simulador
   for k in 1 .. 15 loop

      --Copiamos los valores de la entrada de cada iteracion en la DB
      db(k, 1) := Float(k);
      db(k, 4) := entrada(k, 4); --ST3
      db(k, 5) := entrada(k, 3); --ST4
      db(k, 8) := entrada(k, 2); --SR1

      --Calcularemos primeramente ST2(k)
      st2 := st2K(db(k-1, 2), db(k-1, 3), db(k, 5), db(k, 6), db(k, 8));

      --Calculamos ST1(k)
      st1 := st1K(st2);

      --Calculamos SD1(k)
      sd1 := sd1K(db(k-1, 3), db(k, 4), db(k, 7));

      --Realizamos la verificacion de seguridad
      if st2 > 95.0 then
         Alarma(ST2t, st2);
      elsif db(k,4) > 95.0 then
         Alarma(ST3t, db(k,4));
      end if;

      --Guardamos los datos calculados en DB
      db(k, 2) := st1;
      db(k, 3) := st2;
      db(k, 9) := sd1;


   end loop;

   --Grabamos la Base de Datos en el archivo data.txt
   FileWriter(db, "data.txt");

   Put_Line("Los resultados del simulador son:");
   Put_Line("");

   --Finalmente, mostramos la Base de Datos en consola
   VisualizarSalida(db);

end Main;
