package Simulador is

   function st2K (st1k1, st2k1, st4, sc1, sr1 : Float) return Float;
   function tt (st1k1, st2k1 : Float) return Float;
   function st1K (st2 : Float) return Float;
   function sd1K (st2k1, st3, sc2 : Float) return Float;
   function SC1c (st1k1, st2k1, st4, sr1 : Float) return Float; --Ejercicio 2
   function SC2c (st2k1, st3 : Float) return Float; --Ejercicio 2

private
   --ST2 optimo y SD1 optimo
   st2o : Float := 82.0; --�C
   sd1o : Float := 27.0; --L/h

   --Constantes
   b    : constant Float := 0.13; -- m
   leq  : constant Float := 15.0; -- m
   h    : constant Float := 4.0; -- J/sK
   c    : constant Float := 9.0 * 2.0 * 6.0 * 10000.0; -- s L/min m�
   cp   : constant Float := 4190.0; -- J/kg �C
   p    : constant Float := 975.0; -- kg/m�

end Simulador;
