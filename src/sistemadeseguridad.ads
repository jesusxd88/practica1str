with Ada.Text_IO; use Ada.Text_IO;

package SistemaDeSeguridad is

   type TipoAlarma is (ST2t, ST3t);
   type Fixed is delta 0.001 range -1.06e10 .. 1.06e10;

   procedure Alarma (tipo : TipoAlarma; valor : Float);

end SistemaDeSeguridad;
