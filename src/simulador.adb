package body Simulador is

   ecuacion, primeraParte, segundaParte,
   terceraParte : Float;

   --Calculamos ST2(k)
   function st2K (st1k1, st2k1, st4, sc1, sr1 : Float) return Float is
   begin

      primeraParte := st1k1; --st1k1 y st2k1 son ST1(k-1) y ST2(k-1), respectivamente
      segundaParte := ((b * leq * sr1) * c) / (sc1 * cp * p);
      terceraParte := ((h * (tt (st1k1, st2k1) - st4)) * c) / (sc1 * cp * p);
      ecuacion     := primeraParte + segundaParte - terceraParte;

      return ecuacion;
   end st2K;

   --Calculamos Tt(k)
   function tt (st1k1, st2k1 : Float) return Float is
   begin

      ecuacion := (st1k1 + st2k1) / 2.0;

      return ecuacion;
   end tt;

   --Calculamos ST1(k)
   function st1K (st2 : Float) return Float is
   begin

      ecuacion := st2 - 10.0;

      return ecuacion;
   end st1K;

   --Calculamos SD1(k)
   function sd1K (st2k1, st3, sc2 : Float) return Float is
   begin


      ecuacion := 24.0 * (0.135 + (0.003 * st2k1) - (0.0203 * st3) - (0.001 * sc2)
                          + (0.00004 * st2k1 * sc2));

      return ecuacion;
   end sd1K;

   --Ejercicio 2
   --Calculamos el valor de SC1 dado el valor optimo de ST2
   function SC1c (st1k1, st2k1, st4, sr1 : Float) return Float is
   begin

      ecuacion := (((b * leq * sr1) * c) - ((h * (tt(st1k1, st2k1) - st4)) * c)) / ((st2o - st1k1) * cp * p);

      return ecuacion;

   end SC1c;

   --Calculamos el valor de SC2 dado el valor optimo de ST2 y SD1
   function SC2c (st2k1, st3 : Float) return Float is
   begin

      ecuacion := ((sd1o / 24.0) - (0.135 +0.003 * st2o - 0.0203 * st3)) / (0.00004 * st2o - 0.001);

      return ecuacion;

   end SC2c;


end Simulador;
